import { Component } from '@angular/core';

import { PricesListComponent } from './prices-list/prices-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pricix';
}
