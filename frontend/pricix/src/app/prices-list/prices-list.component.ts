import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Pricing } from '../pricing';
import { PricingService } from '../pricing.service';


@Component({
  selector: 'app-prices-list',
  templateUrl: './prices-list.component.html',
  styleUrls: ['./prices-list.component.css']
})
export class PricesListComponent implements OnInit {

  startTimes = [
    { 'label': 'a day', 'value': 1 },
    { 'label': '5 days', 'value': 2 },
    { 'label': 'a week', 'value': 7 },
    { 'label': 'a month', 'value': 30 },
    { 'label': 'a year', 'value': 365 },
    { 'label': 'custom', 'value': -1 }
  ];
  columnsToDisplay = ['shop', 'product', 'brand', 'regularPrice', 'promoPrice',
    'discount', 'downloadDate'];

  pageSizeOptions = [5, 10, 25, 100];

  daysAgo = 365;
  startDate = new FormControl(new Date());

  resultsLength = 0;
  pricings: Pricing[];

  constructor(private pricingService: PricingService) { }

  ngOnInit() {
    this.getPricings();
  }

  getPricings(max_results = 5, pageIndex = 0): void {
    this.pricingService.getPricings(
      max_results,
      pageIndex + 1,
      this.getStartDate()
    ).subscribe(data => {
      this.resultsLength = data._meta.total;
      this.pricings = data._items;
    });
  }

  getStartDate(): Date {
    let startDate: Date = this.startDate.value;
    if (this.daysAgo !== -1) {
      startDate = new Date();
      startDate.setDate(startDate.getDate() - this.daysAgo);
    }
    return startDate;
  }
}
