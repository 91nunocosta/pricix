export interface Pricing {
    shop: string;
    productName: string;
    brand: string;
    regularPrice: number;
    promoPrice: number;
    discount: number;
    downloadDate: Date;
}
