import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Pricing } from './pricing';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PricingService {

  constructor(private http: HttpClient) { }

  getPricings(max_results = 10, page = 0, start?: Date): Observable<PythonEveAPI<Pricing>> {
    let url = `http://localhost:5000/pricings?max_results=${max_results}&page=${page}`;
    if (start !== undefined) {
      url += `&where=downloadDate>"${start.toUTCString()}"`;
    }
    return this.http.get<PythonEveAPI<Pricing>>(url);
  }
}

export interface PythonEveAPI<T> {
  '_items': T[];
  '_meta': PythonEveMeta;
}

export interface PythonEveMeta {
  'page': number;
  'max_results': number;
  'total': number;
}
