from datetime import datetime
import csv

from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['pricix']
DATA_FILE_PATH = 'acme_promos.csv'

def load_pricings_data(data_file_path):
    with open(data_file_path, encoding='utf-8') as pricings_csv:
        read_csv = csv.reader(pricings_csv, delimiter=',')
        next(read_csv)
        for row in read_csv:
            yield {
                'shop': row[0],
                'productName': row[1],
                'brand': row[2],
                'regularPrice': row[3],
                'promoPrice': row[4],
                'discount': row[5],
                'downloadDate': datetime.strptime(row[6], '%Y-%m-%d %H:%M:%S.%f+00')
            } 

result = db.pricings.insert_many(load_pricings_data(DATA_FILE_PATH))

print('Inserted {} pricings.'.format(len(result.inserted_ids)))