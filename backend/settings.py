# Let's just use the local mongod instance. Edit as needed.

# Please note that MONGO_HOST and MONGO_PORT could very well be left
# out as they already default to a bare bones local 'mongod' instance.
MONGO_HOST = 'localhost'
MONGO_PORT = 27017

# Skip these if your db has no auth. But it really should.
#MONGO_USERNAME = '<your username>'
#MONGO_PASSWORD = '<your password>'

MONGO_DBNAME = 'pricix'

X_DOMAINS = '*'

pricing_schema = {
        'shop': 'string',
        'productName': 'string',
        'brand': 'string',
        'regularPrice': 'number',
        'promoPrice': 'number',
        'discount': 'number',
        'downloadDate': 'datetime'
    }

pricings = {
    'schema': pricing_schema
}

DOMAIN = {'pricings': pricings}